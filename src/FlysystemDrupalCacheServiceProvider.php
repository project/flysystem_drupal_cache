<?php

namespace Drupal\flysystem_drupal_cache;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;
use Drupal\flysystem_drupal_cache\Flysystem\DrupalCachePlugin;
use Symfony\Component\DependencyInjection\Reference;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Class FlysystemDrupalCacheProvider
 *
 * @package Drupal\flysystem_drupal_cache
 */
class FlysystemDrupalCacheServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function register(ContainerBuilder $container) {
    foreach (Settings::get('flysystem', []) as $scheme => $configuration) {
      if ($configuration['driver'] !== 'drupal_cache' || !isset($configuration['config']['bin'])) {
        continue;
      }

      $bin = DrupalCachePlugin::getCacheBin($configuration['config']['bin']);
      $container->register('cache.' . $bin, CacheBackendInterface::class)
        ->addTag('cache.bin')
        ->setFactory([new Reference('cache_factory'), 'get'])
        ->addArgument($bin);
    }
  }
}
