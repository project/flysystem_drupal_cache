<?php

namespace Drupal\flysystem_drupal_cache\Flysystem;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flysystem\Plugin\FlysystemPluginInterface;
use Drupal\flysystem\Plugin\FlysystemUrlTrait;
use Drupal\flysystem_drupal_cache\DrupalCacheAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Drupal plugin for the "Drupal Cache" Flysystem adapter.
 *
 * @Adapter(id = "drupal_cache")
 */
class DrupalCachePlugin implements FlysystemPluginInterface, ContainerFactoryPluginInterface {

  use FlysystemUrlTrait {
    getExternalUrl as getDownloadlUrl;
  }

  /**
   * Cache Backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Cache Tags Invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Mime Type Guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * List of cache tags.
   *
   * @var array
   */
  protected $tags;

  /**
   * DrupalCache constructor.
   *
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cacheFactory
   *   Cache Factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   Cache Tags Invalidator.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mimeTypeGuesser
   *   Cache Tags Invalidator.
   * @param $bin
   *   Cache bin name without flysystem_prefix.
   * @param $tags
   *   List of cache tags.
   */
  public function __construct(CacheFactoryInterface $cacheFactory, CacheTagsInvalidatorInterface $cacheTagsInvalidator, MimeTypeGuesserInterface $mimeTypeGuesser, $bin, $tags) {
    $this->cacheBackend = $cacheFactory->get(self::getCacheBin($bin));
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
    $this->tags = $tags;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('cache_factory'),
      $container->get('cache_tags.invalidator'),
      $container->get('file.mime_type.guesser'),
      $configuration['bin'],
      $configuration['tags']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAdapter() {
    return new DrupalCacheAdapter($this->cacheBackend, $this->cacheTagsInvalidator, $this->mimeTypeGuesser, $this->tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl($uri) {
    $path = str_replace('\\', '/', $this->getTarget($uri));
    return $GLOBALS['base_url'] . '/_flysystem/' . $this->getScheme($uri) . '/' . UrlHelper::encodePath($path);
  }

  /**
   * @inheritDoc
   */
  public function ensure($force = FALSE) {
    return [];
  }

  /**
   * Get the name of the cache bin.
   *
   * @param string $bin
   *   Name of the bin without flysystem_ prefix.
   *
   * @return string
   */
  public static function getCacheBin($bin) {
    return 'flysystem_' . $bin;
  }

}
