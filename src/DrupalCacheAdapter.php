<?php

namespace Drupal\flysystem_drupal_cache;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\StreamedTrait;
use League\Flysystem\AdapterInterface;
use League\Flysystem\Config;
use League\Flysystem\Util;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Class DrupalCacheAdapter
 *
 * @package Drupal\flysystem_drupal_cache
 */
class DrupalCacheAdapter extends AbstractAdapter {

  use StreamedTrait;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Mime type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeTypeGuesser;

  /**
   * Default cache tags.
   * @var array
   */
  protected $tags;

  /**
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cacheTagsInvalidator
   *   Cache tags invalidator.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mimeTypeGuesser
   *   Mime type guesser.
   * @param array $tags
   *   Default cache tags.
   */
  public function __construct(CacheBackendInterface $cache, CacheTagsInvalidatorInterface $cacheTagsInvalidator, MimeTypeGuesserInterface $mimeTypeGuesser, array $tags) {
    $this->cacheBackend = $cache;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
    $this->tags = $tags;
  }

  /**
   * Returns the StorageClient.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   */
  public function getCacheBackend() {
    return $this->cacheBackend;
  }

  /**
   * Get the default tags.
   *
   * @return array
   */
  public function getTags() {
    return $this->tags;
  }

  /**
   * Get the cache tags for the file.
   *
   * @param string $path
   *
   * @return array
   */
  private function getPathTags($path) {
    $tags = array_merge($this->getTags(), [$path]);

    $pathSegments = explode('/', $path);
    $previous = '';
    foreach ($pathSegments as $pathSegment) {
      $tags[] = $previous . $pathSegment;
      $previous = $pathSegment . ':';
    }

    return $tags;
  }

  /**
   * Get the cache tags invalidator.
   *
   * @return \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  private function getCacheTagsInvalidator() {
    return $this->cacheTagsInvalidator;
  }

  /**
   * @inheritDoc
   */
  public function write($path, $contents, Config $config) {
    $data = [
      'mimetype' => $this->mimeTypeGuesser->guess($path),
      'contents' => base64_encode($contents)
    ];
    $this->getCacheBackend()->set($path, $data, Cache::PERMANENT, $this->getPathTags($path));
  }

  /**
   * @inheritDoc
   */
  public function update($path, $contents, Config $config) {
    $this->write($path, $contents, $config);
  }

  /**
   * @inheritDoc
   */
  public function rename($path, $newpath) {
    $this->copy($path, $newpath);
    $this->delete($path);
  }

  /**
   * @inheritDoc
   */
  public function copy($path, $newpath) {
    $cacheItem = $this->getCacheBackend()->get($path);
    $this->getCacheBackend()->set($newpath, $cacheItem->data, $this->getPathTags($path));
  }

  /**
   * @inheritDoc
   */
  public function delete($path) {
    $this->getCacheBackend()->delete($path);
    $this->getCacheTagsInvalidator()->invalidateTags([$path]);
  }

  /**
   * @inheritDoc
   */
  public function deleteDir($dirname) {
    $this->getCacheTagsInvalidator()->invalidateTags([$dirname]);
  }

  /**
   * @inheritDoc
   */
  public function createDir($dirname, Config $config) {
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function setVisibility($path, $visibility) {
    return TRUE;
  }

  /**
   * @inheritDoc
   */
  public function has($path) {
    $cacheItem = $this->getCacheBackend()->get($path);
    return empty(pathinfo($path, PATHINFO_EXTENSION)) || is_object($cacheItem);
  }

  /**
   * @inheritDoc
   */
  public function read($path) {
    $cacheItem = $this->getCacheBackend()->get($path);
    if (!is_object($cacheItem)) {
      return false;
    }

    $data = $cacheItem->data;
    $data['contents'] = base64_decode($data['contents']);

    return $data;
  }

  /**
   * @inheritDoc
   */
  public function listContents($directory = '', $recursive = FALSE) {
    return [];
  }

  /**
   * @inheritDoc
   */
  public function getMetadata($path) {
    if (!$this->has($path)) {
      return FALSE;
    }

    $cacheItem = $this->getCacheBackend()->get($path);

    if (empty($cacheItem->data['contents']) || empty(pathinfo($path, PATHINFO_EXTENSION))) {
      return [
        'type' => 'dir',
        'visibility' => AdapterInterface::VISIBILITY_PUBLIC,
        'timestamp' => \Drupal::time()->getRequestTime(),
      ];
    }

    $contentSize = Util::contentSize(base64_decode($cacheItem->data['contents']));

    return [
      'type' => 'file',
      'size' => $contentSize,
      'mimetype' => $cacheItem->data['mimetype'],
      'timestamp' => $cacheItem->created,
      'visibility' => AdapterInterface::VISIBILITY_PUBLIC,
    ];
  }

  /**
   * @inheritDoc
   */
  public function getSize($path) {
    $this->getMetadata($path);
  }

  /**
   * @inheritDoc
   */
  public function getMimetype($path) {
    $this->getMetadata($path);
  }

  /**
   * @inheritDoc
   */
  public function getTimestamp($path) {
    $this->getMetadata($path);
  }

  /**
   * @inheritDoc
   */
  public function getVisibility($path) {
    $this->getMetadata($path);
  }
}
