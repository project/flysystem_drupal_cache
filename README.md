Flysystem Drupal Cache
====================

## INTRODUCTION ##

[Flysystem](http://flysystem.thephpleague.com/) is a filesystem abstraction
which allows you to easily swap out a local filesystem for a remote one.

This specific module for Drupal implements an integration with the Drupal
Cache layer to store assets **temporarily**. It is not meant for persistent
storage; instead, it is intendedd for usages in the line of image styles,
aggregated css and js. In an horizontal scalable architecture, such as
Kubernetes, you do not want your page cache to serve assets that are not at risk
of not being accessible.

For example, it is possible in Kubernetes to automatically add and
load balance new containers running PHP and Apache while using the
same DB. Your Drupal instance will still be serving page caches with
references to file paths on your containers that might not be there because the
container has just started. To circumvent this, you want to load your assets
from a location that is not limited to the file storage on nodes/containers.

## REQUIREMENTS ##

 This module has dependency on the below mentioned contributed module.
 * flysystem (https://www.drupal.org/project/flysystem):
   Provides a bridge to use Flysystem as Drupal stream wrappers.

## INSTALLATION ##

These are the steps you need to take in order to use this software. Order is
important.

 1. Download and install flysystem drupal cache module and its dependencies.
 2. Install flysystem drupal cache module.

```bash
cd /path/to/drupal/root
composer require drupal/flysystem_drupal_cache
drush en flysystem_drupal_cache
```

 3. Setup the scheme in the settings.
 4. Enjoy.

## TROUBLESHOOTING ##

If you are having trouble with this module, check the status page at
admin/reports/status. The status page runs all the Flysystem checks and provides
useful error reporting.

## CONFIGURATION ##

Stream wrappers are configured in settings.php.

The keys (assets-example below) are the names of the stream wrappers.

For example: 'assets-example://filename.txt'.

Stream wrapper names cannot contain underscores, they can only contain letters,
numbers, + (plus sign), . (period), - (hyphen).

The 'config' is the configuration passed into the Flysystem adapter. The 'bin'
settings must be set to an identifier. For most cases, this could be the same
as the name of your scheme.

Example configuration:

```php
$schemes = [
  'assets-example' => [           // The name of the stream wrapper.

    'driver' => 'drupal_cache',         // The plugin key.

    'config' => [
      'bin' => 'assets', // This will be prefixed with flysystem_, and is the name of the cache bin.

    'name' => 'Custom stream wrapper name', // Defaults to Flysystem: scheme.

    'description' => 'Custom description',  // Defaults to Flysystem: scheme.

    'cache' => TRUE,             // Cache filesystem metadata. Not necessary for
                                 // the local driver.

    'replicate' => 'ftpexample', // 'replicate' writes to both filesystems, but
                                 // reads from this one. Functions as a backup.

    'serve_js' => TRUE,          // Serve Javascript or CSS via this stream wrapper.
    'serve_css' => TRUE,         // This is useful for adapters that function as
                                 // CDNs like the S3 adapter.
    ]
  ]
];

// Don't forget this!
$settings['flysystem'] = $schemes;
```

## ADVANCED CONFIGURATION ##
By using the Drupal cache backend, it is possible to swap the storage on
that level between different cache backend.

By default, the "default" database cache backend is used. This means
that a table is generated in the database: cache_flysystem_\[BIN NAME\]. In the
above example, it would be "cache_flysystem_assets" because of the 'bin' setting
in 'config'.

You can set the cache backend on that bin within Drupal:
```php
$settings['cache']['bins']['flysystem_BIN'] = 'cache.backend.database';
// Or
$settings['cache']['bins']['flysystem_BIN'] = 'cache.backend.redis';
```

In our example case, 'flysystem_BIN' would be 'flysystem_assets'. I would
recommend a backend that does not have a high risk in clearing caches
for some reason, such as a database or Redis.
